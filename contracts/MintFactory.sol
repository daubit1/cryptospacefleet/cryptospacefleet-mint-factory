//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.11;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface ICryptospacefleet {
    function mintTo(address _to, string memory _tokenURI) external;
}

contract MintFactory is AccessControl {
    bytes32 public constant FREEZER_ROLE = keccak256("FREEZER_ROLE");

    IERC20 private immutable USDC;

    ICryptospacefleet private immutable CSF;

    bool private frozen;

    uint256 private price;

    address private payout;

    string private category;

    string[] private cids;

    event Minted(
        address indexed payer,
        uint256 price,
        uint256 amount,
        string category
    );

    constructor(
        address _payout,
        string memory _category,
        address _USDC,
        address _CSF
    ) {
        payout = _payout;
        category = _category;
        USDC = IERC20(_USDC);
        CSF = ICryptospacefleet(_CSF);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(FREEZER_ROLE, msg.sender);
    }

    modifier isFrozen() {
        require(!frozen, "Function is frozen!");
        _;
    }

    function getNFT() public view returns (address) {
        return address(CSF);
    }

    function getToken() public view returns (address) {
        return address(USDC);
    }

    function freeze() external onlyRole(FREEZER_ROLE) {
        frozen = true;
    }

    function unfreeze() external onlyRole(FREEZER_ROLE) {
        frozen = false;
    }

    function getPrice() public view returns (uint256) {
        return price * 10**6;
    }

    function setPrice(uint256 _price) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(_price > 0, "Invalid argument!");
        price = _price;
    }

    function getWallet() external view returns (address) {
        return payout;
    }

    function setWallet(address _payout) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(_payout != address(0), "Invalid address!");
        payout = _payout;
    }

    function getCID(uint256 id)
        external
        view
        onlyRole(DEFAULT_ADMIN_ROLE)
        returns (string memory)
    {
        require(0 <= id && id < totalSupply(), "Invalid id");
        return cids[id];
    }

    function addCID(string memory cid) public onlyRole(DEFAULT_ADMIN_ROLE) {
        require(bytes(cid).length != 0, "Invalid string!");
        cids.push(cid);
    }

    function addCIDs(string[] calldata _cids)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(_cids.length != 0, "Invalid array!");
        for (uint256 i = 0; i < _cids.length; i++) {
            addCID(_cids[i]);
        }
    }

    function removeCID(uint256 id) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _remove(id);
    }

    function _remove(uint256 i) internal {
        require(0 <= i && i < cids.length, "Error: Out of bounds!");
        cids[i] = cids[cids.length - 1];
        cids.pop();
    }

    function totalSupply() public view returns (uint256) {
        return cids.length;
    }

    function mint(uint256 amount) external isFrozen {
        uint256 _total = totalSupply();
        require(0 < amount && amount <= _total, "Insufficient amount!");
        uint256 _price = getPrice();
        uint256 totalCost = amount * _price;
        uint256 allowance = USDC.allowance(msg.sender, address(this));
        require(allowance >= totalCost, "Insufficient allowance!");
        bool success = USDC.transferFrom(msg.sender, payout, totalCost);

        require(success, "Error: Invalid transfer!");
        // First hash to pseudo-randomnly select token
        bytes32 hashValue = keccak256(
            abi.encodePacked(
                msg.sender,
                blockhash(block.number - 1),
                block.number,
                amount
            )
        );
        for (uint256 i = 0; i < amount; i++) {
            uint256 rand = uint256(hashValue) % totalSupply();
            string memory tokenURI = cids[rand];
            CSF.mintTo(msg.sender, tokenURI);
            _remove(rand);
            //Further hashing involves previous token CID to further spread entropy
            hashValue = keccak256(abi.encodePacked(hashValue, tokenURI));
        }
        emit Minted(msg.sender, price, amount, category);
    }

    function getCategory() external view returns (string memory) {
        return category;
    }
}
