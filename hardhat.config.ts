import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import { getPayers, verifyAll, getCids } from "./scripts/task";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "hardhat-deploy";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";

dotenv.config();
const MNEMONIC = process.env.MNEMONIC;
const ALCHEMY_KEY_MAINNET = process.env.ALCHEMY_KEY_MAINNET;
const ALCHEMY_KEY_TESTNET = process.env.ALCHEMY_KEY_TESTNET;
const mumbaiNodeUrl = `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_KEY_TESTNET}`;
const polygonNodeUrl = `https://polygon-mainnet.g.alchemy.com/v2/${ALCHEMY_KEY_MAINNET}`;

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

task("verifyAll", "Verifies every contract", verifyAll);

task("getPayers", getPayers).addParam("from", "Start block height");

task("getCIDs", getCids);

const config: HardhatUserConfig = {
  solidity: "0.8.11",
  networks: {
    development: {
      url: "http://127.0.0.1:8545/",
    },
    mumbai: { url: mumbaiNodeUrl, accounts: { mnemonic: MNEMONIC } },
    polygon: { url: polygonNodeUrl, accounts: { mnemonic: MNEMONIC } },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  mocha: {
    // reporter: "eth-gas-reporter",
    // reporterOptions: {
    //   currency: "USD",
    //   token: "MATIC",
    //   gasPriceApi:
    //     "https://api.polygonscan.com/api?module=proxy&action=eth_gasPrice",
    //   url: "http://localhost:8545",
    // },
  },
  etherscan: {
    apiKey: process.env.POLYSCAN_KEY,
  },
};

export default config;
