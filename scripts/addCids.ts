import { ethers } from "hardhat";
import { IO } from "./util/io";
import { CATEGORIES } from "./util/const";
import { readFileSync } from "fs";

function take<T>(arr: T[], len: number) {
  return arr.splice(0, len);
}

async function main() {
  const io = new IO();
  const [deployer] = await ethers.getSigners();
  const network = await deployer.getChainId();
  const addresses = io.fetchAddresses(network);

  const cidData = JSON.parse(readFileSync("contracts/cids.json", "utf8"));

  for (const category of CATEGORIES) {
    const address = addresses[category];
    console.log(address);
    const Factory = await ethers.getContractFactory("MintFactory");
    if (cidData[category]) {
      const factory = Factory.attach(address);
      const setPrice = await factory.setPrice(cidData[category].price);
      console.log(setPrice.hash);
      await setPrice.wait();
      const cids: string[] = cidData[category].cids.map(
        ([key, value]: [string, string]) => value
      );
      while (cids.length > 0) {
        console.log(cids.length);
        const cidBatch = take(cids, 256);
        console.log(cidBatch);
        const addCids = await factory.addCIDs(cidBatch);
        console.log(addCids.hash);
        await addCids.wait();
      }
    }
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
