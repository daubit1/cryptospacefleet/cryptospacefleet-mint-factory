// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";
import { IO } from "./util/io";

const TOKEN_CDI = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";

async function main() {
  const io = new IO();
  const [deployer] = await ethers.getSigners();
  const network = await deployer.getChainId();
  const { factory: address } = io.fetchAddresses(network);

  const Factory = await ethers.getContractFactory("MintFactory");
  const factory = Factory.attach(address);
  let cids: string[];
  let batchSizes = [250, 260, 270, 280, 290]; //Upper limit 290
  for (let size of batchSizes) {
    console.log("Trying", size);
    cids = new Array(size).fill(TOKEN_CDI, 0, size);
    try {
      const addCIDs = await factory.addCIDs(cids);
      await addCIDs.wait();
      console.log(size, "NFTs added!\n");
    } catch (e) {
      console.log(size, "exceeded!");
    }
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
