/* eslint-disable no-unused-vars */
// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";
import { Contract } from "ethers";
import { IO } from "./util/io";
import { CATEGORIES } from "./util/const";

const PAYOUT_ADDRESS = "0x7a2fb2Eb7aaF5d3E9c00fF478B008FB1925e7B4d";
const CONTRACT_CID = "QmfDBBDeW3K8UbMVPssg769NF6qpiByuswTuGtNTzC19hg";
const REGISTRY_ADDRESS = "0x58807baD0B376efc12F5AD86aAc70E78ed67deaE";
const MINTER_ROLE =
  "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6";

async function main() {
  const io = new IO();
  const [deployer] = await ethers.getSigners();
  const network = await deployer.getChainId();
  const addresses = io.fetchAddresses(network);
  const csfAddress = addresses.Csf;
  const usdcAddress = addresses.Usdc;

  console.log("Deploying contracts with the account:", deployer.address);

  const newAddresses = {};

  let csf: Contract;
  const CSF = await ethers.getContractFactory("Cryptospacefleet");
  if (!csfAddress) {
    throw new Error("No CSF addresses");
    // csf = await upgrades.deployProxy(CSF, [REGISTRY_ADDRESS, CONTRACT_CID]);
    // await csf.deployed();
    // console.log("CSF deployed to:", csf.address);
  } else {
    csf = CSF.attach(csfAddress);
    console.log("Loading CSF contract from", csf.address);
  }
  // @ts-ignore
  newAddresses.Csf = csf.address;

  let usdc: Contract;
  const USDC = await ethers.getContractFactory("USDC");
  if (!usdcAddress) {
    throw new Error("NO USDC address");
    // usdc = await USDC.deploy();
    // await usdc.deployed();
    // console.log("USDC deployed to:", usdc.address);
  } else {
    usdc = USDC.attach(usdcAddress);
    console.log("Loading USDC contract from", usdc.address);
  }
  // @ts-ignore
  newAddresses.Usdc = usdc.address;

  const Factory = await ethers.getContractFactory("MintFactory");
  for (const category of CATEGORIES) {
    let factory: Contract;
    const address = addresses[category];
    if (!address) {
      factory = await Factory.deploy(
        PAYOUT_ADDRESS,
        category,
        usdc.address,
        csf.address
      );
      await factory.deployed();
      console.log(`${category} Factory deployed to:`, factory.address);
      await csf.grantRole(MINTER_ROLE, factory.address);
    } else {
      factory = Factory.attach(address);
      console.log(`Loading ${category} Factory from:`, factory.address);
    }
    // @ts-ignore
    newAddresses[category] = factory.address;
  }
  io.saveAddresses(network, newAddresses);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  // eslint-disable-next-line no-process-exit
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
