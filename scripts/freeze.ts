import { ethers } from "hardhat";
import { IO } from "./util/io";
import { CATEGORIES } from "./util/const";

async function main() {
  const io = new IO();
  const [deployer] = await ethers.getSigners();
  const network = await deployer.getChainId();
  const addresses = io.fetchAddresses(network);

  for (const category of CATEGORIES) {
    const address = addresses[category];
    const Factory = await ethers.getContractFactory("MintFactory");
    const factory = Factory.attach(address);
    const freeze = await factory.freeze();
    await freeze.wait();
    console.log(`freeze ${category} ${address} ${freeze.hash}`);
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
