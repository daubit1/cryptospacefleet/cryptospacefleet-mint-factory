// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";
import { IO } from "./util/io";
import { CATEGORIES } from "./util/const";

// Just for test purposes
const data = {
  Passenger: { price: 100, cid: "Passenger" },
  Crew: { price: 500, cid: "Crew" },
  Officer: { price: 1000, cid: "Officer" },
  Captain: { price: 10000, cid: "Captain" },
} as any;

async function main() {
  const io = new IO();
  const [deployer] = await ethers.getSigners();
  const network = await deployer.getChainId();
  const addresses = io.fetchAddresses(network);

  for (let category of CATEGORIES) {
    const address = addresses[category];
    const { price, cid } = data[category];
    const Factory = await ethers.getContractFactory("MintFactory");
    const factory = Factory.attach(address);
    const setPrice = await factory.setPrice(price);
    await setPrice.wait();
    console.log(`Set ${category} Price to ${price}!`);
    const cids = new Array(10).fill(cid, 0, 10).map((cid, i) => cid + i);
    const addCIDs = await factory.addCIDs(cids);
    await addCIDs.wait();
    console.log(`NFT added for ${category}!`);
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
