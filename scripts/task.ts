/* eslint-disable node/no-missing-import */
import { BigNumber } from "ethers";
import { writeFileSync } from "fs";
import "hardhat/config";
import { HardhatRuntimeEnvironment } from "hardhat/types";
import ProgressBar from "progress";
import { CATEGORIES } from "./util/const";
import { IO } from "./util/io";

export const verifyAll = async (args: any, hre: any) => {
  const io = new IO();
  const addresses = io.fetchAddresses(137);
  /**
   * npx hardhat verify 0x80Fb4c1DfAdCBa5bEcb259038e968156A7CCeBAd --network mumbai 0xBFDC28B7bce28EFF0cC1361F38A538aCdE5a28e6 Kitty 0xdB46757567DcC2E2Abf6df3459e45083a6189afd 0x849E185079663DDbECB4aFe4a9cA6d702869D74F
   */
  const USDC = addresses.Usdc;
  const CSF = addresses.Csf;
  const PAYOUT = "0x7a2fb2Eb7aaF5d3E9c00fF478B008FB1925e7B4d";
  for (const category of CATEGORIES) {
    const address = addresses[category];
    try {
      await hre.run("verify", {
        address,
        network: "polygon",
        constructorArgsParams: [PAYOUT, category, USDC, CSF],
      });
    } catch (e) {
      console.log(`${category} already verified!`);
    }
  }
};

export const getPayers = async (args: any, hre: HardhatRuntimeEnvironment) => {
  const io = new IO();
  const TOPIC =
    "0xf2cb5e52049d127ad1c335f1cc25f2fdbc911bec1beb2611f4c1e8b1c274d4b4";
  const { ethers } = hre;
  const network = await ethers.provider.getNetwork();
  const { chainId } = network;
  const addresses = io.fetchAddresses(chainId);
  const address = addresses.Kitty;
  const { from } = args;
  const logs = await ethers.provider.getLogs({
    fromBlock: Number(from),
    toBlock: "latest",
    topics: [TOPIC],
    address: address,
  });
  const logAddresses = logs
    .map((log) => log.topics[1])
    .map(
      (address) => `0x${address.slice(address.length - 40, address.length)}`
    );
  writeFileSync("data/addresses.json", JSON.stringify(logAddresses, null, 2));
  console.log("Successfully saved!");
};

const sleep = async (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const getCids = async (args: any, hre: HardhatRuntimeEnvironment) => {
  const io = new IO();
  const { ethers } = hre;
  const network = await ethers.provider.getNetwork();
  const { chainId } = network;
  const addresses = io.fetchAddresses(chainId);
  const address = addresses.Kitty;
  const Factory = await ethers.getContractFactory("MintFactory");
  const factory = Factory.attach(address);
  const totalSupply: BigNumber = await factory.totalSupply();
  const cids = [];
  const bar = new ProgressBar("Fetching [:bar] :percent :etas", {
    total: totalSupply.toNumber(),
    width: 30,
  });
  console.log(`Fetching ${totalSupply} CIDs`);
  for (let i = 0; i < totalSupply.toNumber(); i++) {
    await sleep(500);
    const cid: string = await factory.getCID(i);
    cids.push(cid);
    bar.tick();
  }
  writeFileSync("data/cids.json", JSON.stringify(cids, null, 2));
  console.log("Successfully saved!");
};
