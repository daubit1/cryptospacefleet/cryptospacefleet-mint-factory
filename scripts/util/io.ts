import { readFileSync, writeFileSync } from "fs";

export class IO {
  path = "contracts/addresses.json";
  addresses: any;

  fetchAddresses(network: number) {
    this.addresses = JSON.parse(readFileSync(this.path, "utf8"));
    return this.addresses[network.toString()] || {};
  }

  saveAddresses(network: number, addresses: any) {
    this.addresses[network] = { ...addresses };
    const result = JSON.stringify(this.addresses, null, 2);
    writeFileSync(this.path, result);
  }
}
