import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { Contract } from "ethers";
import { ethers, upgrades } from "hardhat";

const CATEGORY = "Kitty";
const PAYOUT_ADDRESS = "0x7a2fb2Eb7aaF5d3E9c00fF478B008FB1925e7B4d";
const NULL_ADDRESS = "0x0000000000000000000000000000000000000000";
const CONTRACT_CID = "QmfDBBDeW3K8UbMVPssg769NF6qpiByuswTuGtNTzC19hg";
const REGISTRY_ADDRESS = "0x58807baD0B376efc12F5AD86aAc70E78ed67deaE";
const MINTER_ROLE =
  "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6";
const FREEZER_ROLE =
  "0x92de27771f92d6942691d73358b3a4673e4880de8356f8f2cf452be87e02d363";
const ADMIN_ROLE =
  "0x0000000000000000000000000000000000000000000000000000000000000000";
const TOKEN_CDI = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";

const CORRECT_PRICE = 15;
const LESSER_PRICE = 14;

describe("Mint Factory", async () => {
  let csf: Contract;
  let usdc: Contract;
  let factory: Contract;
  let admin: SignerWithAddress;
  let userA: SignerWithAddress;
  let userB: SignerWithAddress;
  let factorySupply = 0;
  before(async () => {
    // Setting up accounts
    const [account1, account2, account3] = await ethers.getSigners();
    admin = account1; //Admin & Freezer
    userA = account2; // Freezer
    userB = account3; // User

    // Deploy CSF
    const CSF = await ethers.getContractFactory("Cryptospacefleet");
    csf = await upgrades.deployProxy(CSF, [REGISTRY_ADDRESS, CONTRACT_CID]);
    await csf.deployed();

    //Deploy USDC
    const USDC = await ethers.getContractFactory("USDC");
    usdc = await USDC.deploy();
    await usdc.deployed();

    //Deploy Factory
    const Factory = await ethers.getContractFactory("MintFactory");
    factory = await Factory.deploy(
      PAYOUT_ADDRESS,
      CATEGORY,
      usdc.address,
      csf.address
    );
    await factory.deployed();

    // Granting Minter role to factory
    await csf.grantRole(MINTER_ROLE, factory.address);
    await factory.connect(admin).grantRole(FREEZER_ROLE, userA.address);
  });
  describe("Roles", async () => {
    it("Admin has admin role", async () => {
      expect(await factory.hasRole(ADMIN_ROLE, admin.address)).to.be.true;
    });
    it("Admin has freezer role", async () => {
      expect(await factory.hasRole(FREEZER_ROLE, admin.address)).to.be.true;
    });
    it("Freezer has freezer role", async () => {
      expect(await factory.hasRole(FREEZER_ROLE, userA.address)).to.be.true;
    });
    it("User has not admin role", async () => {
      expect(await factory.hasRole(ADMIN_ROLE, userB.address)).to.be.false;
    });
    it("UserB has not freezer role", async () => {
      expect(await factory.hasRole(FREEZER_ROLE, userB.address)).to.be.false;
    });
  });
  describe("Price", async () => {
    it("Admin can change price", async () => {
      const setPrice = await factory.setPrice(CORRECT_PRICE);
      // wait until the transaction is mined
      await setPrice.wait();
      expect(await factory.getPrice()).to.equal(CORRECT_PRICE * 10 ** 6);
    });
    it("Admin can NOT change to an invalid price", async () => {
      expect(factory.connect(userA).setPrice(-1)).to.be.reverted;
    });
    it("Non-admin can NOT change price", async () => {
      expect(factory.connect(userA).setPrice(LESSER_PRICE)).to.be.reverted;
    });
  });
  describe("CID", async () => {
    describe("Add", () => {
      it("Admin should be able to add a CID", async () => {
        const addCID = await factory.addCID(TOKEN_CDI);
        factorySupply += 1;
        // wait until the transaction is mined
        await addCID.wait();
        expect(await factory.getCID(0)).to.equal(TOKEN_CDI);
      });
      it("Admin can NOT add an empty string", async () => {
        expect(factory.connect(admin).addCID("")).to.be.reverted;
      });
      it("Non-admin should NOT be able to add a CID", async () => {
        expect(factory.connect(userA).addCID(TOKEN_CDI)).to.be.reverted;
      });
      it("Admin should be able to add multiple CIDs", async () => {
        const addCID = await factory.addCIDs([
          TOKEN_CDI,
          TOKEN_CDI,
          TOKEN_CDI,
          TOKEN_CDI,
          TOKEN_CDI,
          TOKEN_CDI,
        ]);
        factorySupply += 6;
        // wait until the transaction is mined
        await addCID.wait();
        expect(await factory.totalSupply()).to.equal(7);
      });
      it("Admin should NOT be able to add empty array", async () => {
        expect(factory.connect(admin).addCIDs([])).to.be.reverted;
      });
      it("Non-admin should NOT be able to add multiple CIDs", async () => {
        expect(factory.connect(userA).addCIDs([TOKEN_CDI])).to.be.reverted;
      });
    });
    describe("Remove", () => {
      it("Admin can remove CID", async () => {
        const preAmount = factorySupply;
        const addCID = await factory.connect(admin).addCID(TOKEN_CDI);
        await addCID.wait();
        const removeCid = await factory.connect(admin).removeCID(0);
        await removeCid.wait();
        expect(await factory.totalSupply()).to.equal(preAmount);
      });
      it("Admin can NOT remove by passing invalid index", async () => {
        expect(factory.connect(admin).removeCID(factorySupply + 1)).to.be
          .reverted;
        expect(factory.connect(admin).removeCID(-1)).to.be.reverted;
      });
      it("Non-admin can NOT remove CID", async () => {
        expect(factory.connect(userA).removeCID(0)).to.be.reverted;
      });
    });
    describe("Get", () => {
      it("Admin can retrieve CID", async () => {
        const getCID = await factory.connect(admin).getCID(0);
        expect(getCID).to.equal(TOKEN_CDI);
      });
      it("Admin can NOT retrieve non-existing CID", async () => {
        expect(factory.connect(admin).getCID(-1)).to.be.reverted;
        expect(factory.connect(admin).getCID(factorySupply + 1)).to.be.reverted;
      });
      it("Non-admin can NOT retrieve existing CID", async () => {
        expect(factory.connect(userA).getCID(0)).to.be.reverted;
      });
    });
  });
  describe("Mint", async () => {
    it("User can NOT mint when not approved", () => {
      expect(factory.connect(userA).mint(1)).to.be.reverted;
    });
    it("User can NOT mint when passing an incorrect amount", async () => {
      expect(factory.connect(userA).mint(-1)).to.be.reverted;
      expect(factory.connect(userA).mint(factorySupply + 1)).to.be.reverted;
    });
    it("User can mint when approved correct amount of tokens", async () => {
      const amount = CORRECT_PRICE * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, amount);
      await transfer.wait();
      // Approve factory contract to spend amount
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, amount);
      await approve.wait();
      // User mints successfully
      const mint = await factory.connect(userA).mint(1);
      factorySupply -= 1;
      await mint.wait();
      expect(await csf.balanceOf(userA.address)).to.be.equal(1);
      expect(await factory.totalSupply()).to.equal(factorySupply);
    });
    it("User can NOT mint when approving wrong amount of tokens", async () => {
      // Price is set to 14
      const amount = LESSER_PRICE * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, amount);
      await transfer.wait();
      // Approve factory contract to spend amount
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, amount);
      await approve.wait();
      // User mints successfully
      expect(factory.connect(userA).mint(1)).to.be.reverted;
    });
    it("User can mint when approving more amount of tokens", async () => {
      // Price is set to 16
      const amount = (CORRECT_PRICE + 1) * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, amount);
      await transfer.wait();
      // Approve factory contract to spend amount
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, amount);
      await approve.wait();
      // User mints successfully
      const mint = await factory.connect(userA).mint(1);
      factorySupply -= 1;
      await mint.wait();
      expect(await csf.balanceOf(userA.address)).to.be.equal(2);
      expect(await factory.totalSupply()).to.equal(factorySupply);
    });
    it("User can mint multiple tokens when approved correct amount of tokens", async () => {
      const amount = 2;
      const totalCosts = amount * CORRECT_PRICE * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, totalCosts);
      await transfer.wait();
      // Approve factory contract to spend totalCosts
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, totalCosts);
      await approve.wait();
      // User mints successfully
      const mint = await factory.connect(userA).mint(amount);
      factorySupply -= amount;
      await mint.wait();
      expect(await csf.balanceOf(userA.address)).to.be.equal(4);
      expect(await factory.totalSupply()).to.equal(factorySupply);
    });
    it("User can mint multiple tokens when approved more amount of tokens", async () => {
      const amount = 2;
      const totalCosts = amount * (CORRECT_PRICE + 1) * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, totalCosts);
      await transfer.wait();
      // Approve factory contract to spend totalCosts
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, totalCosts);
      await approve.wait();
      // User mints successfully
      const mint = await factory.connect(userA).mint(amount);
      factorySupply -= amount;
      await mint.wait();
      expect(await csf.balanceOf(userA.address)).to.be.equal(6);
      expect(await factory.totalSupply()).to.equal(factorySupply);
    });
    it("User can NOT mint multiple tokens when approving incorrect amount of tokens", async () => {
      const amount = 2;
      const totalCosts = amount * LESSER_PRICE * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, totalCosts);
      await transfer.wait();
      // Approve factory contract to spend totalCosts
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, totalCosts);
      await approve.wait();
      // User mint fails
      expect(factory.connect(userA).mint(amount)).to.be.reverted;
    });
  });
  describe("Freeze", async () => {
    it("Admin can freeze mint function", async () => {
      const freeze = await factory.connect(admin).freeze();
      await freeze.wait();
      expect(factory.connect(admin).mint(0)).to.be.reverted;
      expect(factory.connect(userA).mint(0)).to.be.reverted;
    });

    it("Non-admin can NOT freeze mint function", async () => {
      expect(factory.connect(userB).freeze()).to.be.reverted;
    });

    it("Admin can unfreeze mint function", async () => {
      const unfreeze = await factory.connect(admin).unfreeze();
      await unfreeze.wait();
    });
    it("Non-admin can NOT unfreeze mint function", async () => {
      expect(factory.connect(userB).unfreeze()).to.be.reverted;
    });
    it("Freezer (UserA) can freeze mint function", async () => {
      const freeze = await factory.connect(userA).freeze();
      await freeze.wait();
      expect(factory.connect(admin).mint(0)).to.be.reverted;
      expect(factory.connect(userA).mint(0)).to.be.reverted;
    });
    it("Freezer (UserA) can unfreeze mint function", async () => {
      const unfreeze = await factory.connect(userA).unfreeze();
      await unfreeze.wait();
    });
  });

  describe("Wallet", async () => {
    it("Admin can change payout wallet address", async () => {
      const changeWallet = await factory
        .connect(admin)
        .setWallet(userB.address);
      await changeWallet.wait();
      const amount = CORRECT_PRICE * 10 ** 6;
      // Feed user with tokens
      const transfer = await usdc
        .connect(admin)
        .transfer(userA.address, amount);
      await transfer.wait();
      // Approve factory contract to spend amount
      const approve = await usdc
        .connect(userA)
        .approve(factory.address, amount);
      await approve.wait();
      // User mints successfully
      const mint = await factory.connect(userA).mint(1);
      factorySupply -= 1;
      await mint.wait();
      expect(await csf.balanceOf(userA.address)).to.be.equal(7);
      expect(await factory.totalSupply()).to.equal(factorySupply);
      expect(await usdc.balanceOf(userB.address)).to.be.equal(amount);
    });
    it("Admin can NOT change wallet address to NULL address", async () => {
      expect(factory.connect(userA).setWallet(NULL_ADDRESS)).to.be.reverted;
    });
    it("Non-admin can NOT change wallet address", async () => {
      expect(factory.connect(userA).setWallet(userA.address)).to.be.reverted;
    });
  });
});
